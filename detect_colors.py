#!/opt/local/bin/python2.7

from PIL import Image, ImageStat
import sys
import os

MONOCHROMATIC_MAX_VARIANCE = 0.005
COLOR = 1000
MAYBE_COLOR = 100

def detect_color_image(file):
  try:
    v = ImageStat.Stat(Image.open(file)).var
    is_monochromatic = reduce(lambda x, y: x and y < MONOCHROMATIC_MAX_VARIANCE, v, True)
    #print file, '-->\t',
    if is_monochromatic:
        return "Monochromatic image",
    else:
        if len(v)==3:
            maxmin = abs(max(v) - min(v))
            if maxmin > COLOR:
                return "Color"
            elif maxmin > MAYBE_COLOR:
                return "Maybe color"
            else:
                return "grayscale"
            #print "(",maxmin,")"
        elif len(v)==1:
            return "Black and white"
        else:
            return "Don't know"
  except IOError as e:
    return "File error"

if __name__=='__main__':
    #infile = open('train.csv')
    #outfile = open('detected_colors.csv', 'w')
    #infile.readline() #throw away header
    #for line in infile:
      #blobs = line.strip().split(',')
      #print >>outfile, '%s,%s,%s,%s' %(blobs[0], blobs[1], blobs[2], detect_color_image("train/" + blobs[0]))
    #outfile.close()
    input_dir = sys.argv[1]
    out_file = open(sys.argv[1] + ".day_night","w")
    for file in os.listdir(input_dir):
        print >>out_file, '%s\t%s' %(file, detect_color_image(input_dir + "/" + file))
    out_file.close()
    

#! /opt/local/bin/python2.7

import sys


#linear isotonic function: basically, find which bucket a point belongs to in old_intervals, and linearly interpolate its position in the correpsonding bucket in new_intervals
def calibrateProb(prob):
	old_intervals = [ 1., .981, .64, .11, .03, 0.]
	new_intervals = [ .995, .95, .91, .79, .60, 0.]
	score = prob
	i = 1 #assume left endpoint of old is 1---can't be larger
        while score < old_intervals[i] and i < len(old_intervals):
                i+=1
                #i is index of right interval boundary
        newScore = (new_intervals[i-1] - new_intervals[i]) * (score - old_intervals[i])/(old_intervals[i-1]-old_intervals[i]) + new_intervals[i]
	return newScore

if __name__ == '__main__':
	out = open(sys.argv[1], 'w')

	for line in open(sys.argv[2], 'r'):
		blobs = line.strip().split()
		score = float(blobs[2])
		#i = 1 #assume left endpoint of old is 1---can't be larger
		#while score < old_intervals[i] and i < len(old_intervals):
		#	i+=1
		#	#i is index of right interval boundary
		#newScore = (new_intervals[i-1] - new_intervals[i]) * (score - old_intervals[i])/(old_intervals[i-1]-old_intervals[i]) + new_intervals[i]
		#print >>out, "%s\t%s\t%f\t%f\t%f" %(blobs[0], blobs[1], newScore, new_intervals[i-1], new_intervals[i])
		newScore = calibrateProb(score)
		print >>out, "%s %s %f" %(blobs[0], blobs[1], newScore)
	

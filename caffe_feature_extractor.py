#! /usr/bin/env python2.7

import caffe
from PIL import Image
from PIL.ImageOps import mirror
import sys
import uuid
import os
import shutil
import numpy as np
import math
#import pdb

net = caffe.Net('bvlc_caffenet_full_conv.prototxt', 'bvlc_caffenet_full_conv.caffemodel', caffe.TEST)

#usage: extract_caffe_features output_file.lsvm label input_dir file_list (optional)

ofile = open(sys.argv[1], 'w')
label = sys.argv[2]
img_dir = sys.argv[3]
temp_folder = str(uuid.uuid4())
os.mkdir(temp_folder, 0700)
file_list = [line.strip() for line in open(sys.argv[4])] if len(sys.argv)>4 else os.listdir(img_dir)

filenames = [('full.jpg', (600,400)), ('full_mirror.jpg', (600,400)), ('half.jpg', (341,227)), ('half_mirror.jpg', (341,227)), ('double.jpg',(1200,800)), ('double_mirror.jpg',(1200,800))] 
#('quad.jpg', (2400,1600)), ('quad_mirror.jpg', (2400,1600))]

mean_pixel = np.load('ilsvrc_2012_mean.npy').mean(1).mean(1)

for file in file_list:
	try:
		rpath = img_dir + "/" + file
		im = Image.open(rpath)
		imw,imh = im.size
		im_crop = 0
		if imw >= 600 and imh >= 400:
			im_crop = im.crop((0,0,600,400))
		else:
			if .66 * imw > imh:
				im_crop = im.crop((0,0,imh + imh/2, imh)).resize((600,400), Image.BICUBIC)
			else:
				im_crop = im.crop(0,0,imw, math.floor(.66*imw)).resize((600,400), Image.BICUBIC)
		im_crop.save(temp_folder + '/full.jpg')
		mirror(im_crop).save(temp_folder + '/full_mirror.jpg')
		im_small = im_crop.resize((341,227), Image.LANCZOS)
		im_small.save(temp_folder + '/half.jpg')
		mirror(im_small).save(temp_folder + '/half_mirror.jpg')
		im_tiny = im_crop.resize((150,100), Image.LANCZOS)
		im_tiny.save(temp_folder + '/tiny.jpg')
		mirror(im_tiny).save(temp_folder + '/tiny_mirror.jpg')
		im_double = im.resize((1200,800), Image.BICUBIC)
		im_double.save(temp_folder + '/double.jpg')
		mirror(im_double).save(temp_folder + '/double_mirror.jpg')
		im_quad = im.resize((2400,1600), Image.BICUBIC)
		im_quad.save(temp_folder + '/quad.jpg')
		mirror(im_quad).save(temp_folder + '/quad_mirror.jpg')

		feature_acc = []
		for fname, im_size in filenames:
			#pdb.set_trace()
			rpath = temp_folder + '/' + fname
			im = caffe.io.load_image(rpath)
			transformer = caffe.io.Transformer({'data': (1,3, im_size[1], im_size[0])})
			transformer.set_mean('data', mean_pixel)
			transformer.set_transpose('data', (2,0,1))
			transformer.set_channel_swap('data', (2,1,0))
			transformer.set_raw_scale('data', 255.0)
			net.blobs['data'].reshape(1,3, im_size[1], im_size[0])
			net.reshape()
			out = net.forward(data=np.asarray([transformer.preprocess('data', im)]))
			deep_feats = net.blobs['fc8-conv'].data.ravel()
			feature_acc = feature_acc + list(deep_feats)
	
		print >> ofile, label + ' ' + ' '.join(['%d:%.5f' %(id+1,val) for id,val in enumerate(feature_acc)])
	except KeyboardInterrupt:
		raise
	except SystemExit:
		sys.exit()
	except Exception as e:
		print(e)
		
	
ofile.close()
shutil.rmtree(temp_folder)

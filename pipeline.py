#!/opt/local/bin/python2.7

"""pipeline.py out_file in_path.  Makes a prediction for the spotted cat challenge.  Predictions are written (image_file_name \t prediction) to out_file.  in_path should be a directory containing the images to be predicted.  This code probably has optimization issues.  Predictions are: -1=image rejected (not daytime), otherwise, the probability of the image being a spotted cat is given.  (Anything > .5 should be accepted.)  This script requires setting some variables for your installation of Overfeat and Liblinear."""

overfeat_bin='/Users/U0159515/src/Overfeat/bin/macos/overfeat'
#overfeat_bin='/tmp/overfeat/bin/macos/overfeat'
liblinear_home='/Users/U0159515/src/liblinear-1.94'

import sys
import os
import detect_colors
import subprocess
import uuid
import pdb
from convert_prob import calibrateProb

scores = open(sys.argv[1], 'w')
batchsz=200

img_dir = sys.argv[2]

batch_counter=0
night_batch_counter = 0
batch_file_list = []
night_batch_file_list = []
rpath_list = []
night_rpath_list = []

def do_extraction(rpath_list, batch_file_list, isColor):
	global overfeat_bin
	global liblinear_home
	batch_lsvm_file_name = str(uuid.uuid4())
	global scores
	
	batch_lsvm = open(batch_lsvm_file_name,'w')
	if isColor:
		p = subprocess.Popen("convert %s -crop 600x400+0+0 ppm:- | %s -p -f" %(" ".join(rpath_list), overfeat_bin), stderr=sys.stderr, shell=True, stdout=subprocess.PIPE)
	else:
		p = subprocess.Popen("convert %s -colorspace Gray -crop 600x400+0+0 ppm:- | %s -p -f" %(" ".join(rpath_list), overfeat_bin), stderr=sys.stderr, shell=True, stdout=subprocess.PIPE)
	alternator = 0
	for line in p.stdout:
		if alternator:
			blobs = line.strip().split(" ")
			lsvmString = "2 " + " ".join(['%d:%s' %(id+1, val) for id,val in enumerate(blobs)]) #dummy label 2
			print >>batch_lsvm, '%s' %lsvmString
			alternator=0
		else:
			alternator=1
	batch_lsvm.flush()
	if isColor:
		psvm = subprocess.call("%s/predict -b 1 %s model.liblinear %s_preds" %(liblinear_home, batch_lsvm_file_name, batch_lsvm_file_name), stderr=sys.stderr, shell=True, stdout=subprocess.PIPE)
	else:
		psvm = subprocess.call("%s/predict -b 1 %s gray_model.liblinear %s_preds" %(liblinear_home, batch_lsvm_file_name, batch_lsvm_file_name), stderr=sys.stderr, shell=True, stdout=subprocess.PIPE)
	res = open(batch_lsvm_file_name + "_preds", 'r')
	res.readline() #throw away header
	if isColor:
		resString = ['%s\t%s\t1' %(fname, score.strip().split(" ")[1]) for fname,score in zip(batch_file_list,res)]
	else:
		resString = ['%s\t%g\t0' %(fname, calibrateProb(float(score.strip().split(" ")[1]))) for fname,score in zip(batch_file_list,res)]
	for lline in resString:
		print >>scores, '%s' %lline
	os.remove(batch_lsvm_file_name)
	os.remove(batch_lsvm_file_name + "_preds")	
	


for file in os.listdir(img_dir):
	#do day/night classifier
	rpath = img_dir + "/" + file
	dayNightRes = detect_colors.detect_color_image(rpath)
	if dayNightRes!="Color" and dayNightRes!="Maybe color":
		night_batch_file_list.append(file)
		night_rpath_list.append(rpath)
		night_batch_counter+=1
		if night_batch_counter==batchsz:
			do_extraction(night_rpath_list, night_batch_file_list, False)
			night_batch_counter=0
			night_batch_file_list=[]
			night_rpath_list = []
		
	else:
		#do the feature extraction in batch
		batch_file_list.append(file)
		rpath_list.append(rpath)
		batch_counter+=1
		if batch_counter==batchsz:
			do_extraction(rpath_list, batch_file_list, True)
			batch_counter = 0
			batch_file_list = []
			rpath_list = []

if batch_counter>0:
	do_extraction(rpath_list, batch_file_list, True)
if night_batch_counter>0:
	do_extraction(night_rpath_list, night_batch_file_list, False)
